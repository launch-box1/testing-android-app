import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";

export default function App() {
  const Stack = createStackNavigator();
  const handlePress = () => {
    console.log("Button pressed!");
  };
  return (
    <View style={styles.container}>
      <Text style={styles.heading}>MaidSimpl</Text>
      <View style={styles.halfBackground}>
        <Text style={styles.text}>
          This is Next Screen
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  halfBackground: {
    position: "absolute",
    top: "70%",
    left: 0,
    height: "30%",
    width: "100%",
    backgroundColor: "red",
    alignItems: "center",
    justifyContent: "center",
    borderTopRightRadius: 50,
    borderTopLeftRadius: 50,
  },
  text: {
    marginTop: 40,
    fontSize: 24,
    textAlign: "center",
    fontWeight: "500",
    marginBottom: 40,
    color: "white",
  },
  heading: {
    marginTop: -150,
    fontSize: 60,
    textAlign: "center",
    fontWeight: "800",
    marginBottom: 40,
    color: "black",
  },
  buttonText: {
    backgroundColor: "black",
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 15,
    paddingBottom: 15,
    color: "white",
    fontSize: 24,
    fontWeight: "900",
    borderRadius: 20,
  },
});
